import picamera

with picamera.PiCamera() as camera:
    camera.resolution = (320, 240)
    print camera.framerate
    camera.framerate = 15
    camera.start_recording('h264.mp4')
    camera.wait_recording(10)
    camera.stop_recording()

    # camera.start_recording('mjpeg.mp4', format='mjpeg')
    # camera.wait_recording(10)
    # camera.stop_recording()
    #
    # camera.start_recording('yuv.mp4', format='yuv')
    # camera.wait_recording(10)
    # camera.stop_recording()
    #
    # camera.start_recording('rgb.mp4', format='rgb')
    # camera.wait_recording(10)
    # camera.stop_recording()
    #
    # camera.start_recording('bgr.mp4', format='bgr')
    # camera.wait_recording(10)
    # camera.stop_recording()
