// * Name: ofApp.cpp
// * Project: People Counter
// * Author: Wester de Weerdt
// * Creation Date: 04-03-2016

#include "ofApp.h"

using namespace ofxCv;
using namespace cv;
//--------------------------------------------------------------
void ofApp::loadConfig()
{
    cout << "Loading Settings" << endl;
    if (XML.loadFile("config.xml")) {
        _debug = XML.getValue("CONFIG:DEBUG", 0);
        _contourThreshold = XML.getValue("CONFIG:CONTOURTHRESHOLD", 128);
        _threshold = XML.getValue("CONFIG:THRESHOLD", 150);
        _blur = XML.getValue("CONFIG:BLUR", 15);
        _minArea = XML.getValue("CONFIG:MINAREA", 3000);
        _maxArea = XML.getValue("CONFIG:MAXAREA", 256000);
        _persistance = XML.getValue("CONFIG:PERSISTANCE", 5);
        _maxDistance = XML.getValue("CONFIG:MAXDISTANCE", 140);
        _minBlobSize = XML.getValue("CONFIG:MINBLOBWIDTH", 50);
        _midBlobSize = XML.getValue("CONFIG:MIDBLOBWIDTH", 150);
        _maxBlobSize = XML.getValue("CONFIG:MAXBLOBWIDTH", 250);
        _history = XML.getValue("CONFIG:HISTORY", 150);
        _MOGThreshold = XML.getValue("CONFIG:MOGTHRESHOLD", 240);
        _lineYPos = XML.getValue("CONFIG:LINEYPOS", 240/2);
        _lineWidth = XML.getValue("CONFIG:LINEWIDTH", 20);
        _cameraWidth = XML.getValue("CONFIG:CAMERAWIDTH", 320);
        _cameraHeight = XML.getValue("CONFIG:CAMERAHEIGHT", 240);
		_dbName = XML.getValue("CONFIG:Local_dbFile", "counterDB.sqlite");
        _secretKey = XML.getValue("CONFIG:SECRETKEY", "");
        _lightenAmount = XML.getValue("CONFIG:LIGHTENAMOUNT",35);
        _contrast = XML.getValue("CONFIG:CONTRAST",0);
        _brightness = XML.getValue("CONFIG:BRIGHTNESS",0);
        _log_file_name = XML.getValue("CONFIG:Log_FileName","log.txt");
        _log_level = XML.getValue("CONFIG:Log_level",2);

        _source = XML.getValue("CONFIG:SOURCE", 1);
        _video_file_name = XML.getValue("CONFIG:VIDEO_FILE_NAME", "1.mp4");

        _speed = XML.getValue("CONFIG:PLAY_SPEED", 0.5);

        if (_debug > 0){
            if (_source == 0){
                cout << "debugging mode : " << _debug << endl;
                cout << "Video File Name : " << _video_file_name << endl;
            }
        }

        XML.pushTag("CONFIG:MASKPOINTS");
        XML.pushTag("MASKPOINTS:POINT");
        int nbPoints = XML.getNumTags("POINT");
        for (int i = 0; i < nbPoints; i++) {
            int x, y;
            x = XML.getAttribute("POINT", "x", 0, i);
            y = XML.getAttribute("POINT", "y", 0, i);
            _maskPoints.push_back(cv::Point(x, y));
        }
        XML.popTag();
        XML.popTag();
        cout << "Loaded Settings" << endl;
    }
    else {
        cout << "Not Loaded Settings: Program will not run!" << endl;
        exit();
    }
}
//--------------------------------------------------------------
void ofApp::makeMask()
{
    mask = cvCreateMat(240, 320, CV_8UC1);
    for(int i = 0; i < mask.cols; i++)
        for(int j = 0; j < mask.rows; j++)
            mask.at<uchar>(cv::Point(i,j)) = 0;
    
    vector<cv::Point> polyright;
    approxPolyDP(_maskPoints, polyright, 1.0, true);
    
    fillConvexPoly(mask,&polyright[0],polyright.size(),255,8,0);
}
//--------------------------------------------------------------
void ofApp::setup()
{
    loadConfig();

    ofSetFrameRate(30);

	switch(_log_level){
	    case 0: ofSetLogLevel(OF_LOG_VERBOSE); break;
	    case 1: ofSetLogLevel(OF_LOG_NOTICE); break;
	    case 2: ofSetLogLevel(OF_LOG_WARNING); break;
	    case 3: ofSetLogLevel(OF_LOG_ERROR); break;
	    case 4: ofSetLogLevel(OF_LOG_FATAL_ERROR); break;
	    case 5: ofSetLogLevel(OF_LOG_SILENT); break;
	    default : ofSetLogLevel(OF_LOG_WARNING); break;
	}

	ofLogToFile(_log_file_name, true);

	cout << "Log File Name : " << _log_file_name << endl;
	string log_list[6] = {"Verbose", "Notice", "Warning", "Error", "Fatal Error", "Silent"};
	cout << "Log Level : " << log_list[_log_level] << endl;

    count = 0;
    countIn = 0;
    countOut = 0;
    countInLatch = false;
    countOutLatch = false;
    

    makeMask();
    
    startLine = ofPoint(0,_cameraHeight/2);
    endLine = ofPoint(_cameraWidth,_cameraHeight/2);
    
    for (int i = 0; i < 30; i++) {
        counterLatches[i] = true;
    }
    
	setup_SQLite();
    setupCV();

    bTrack = false;
}

//
void ofApp::setup_SQLite()
{
	// get db filename
	std::string myDB_name = ofToDataPath(_dbName, true);
	
	// Open a database file in create/write mode
	myDB = new SQLite::Database(myDB_name, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE);
	
//	ofLogNotice() << "SQLite database file '" << myDB->getFilename() << "' opened successfully";
	
	// creat table if not exists
	try
	{
		myDB->exec("CREATE TABLE IF NOT EXISTS counters (" \
			" id INTEGER PRIMARY KEY AUTOINCREMENT" \
			" ,count INTEGER" \
			" ,direction TEXT" \
			" ,time TEXT" \
			");");
	}
	catch (std::exception& e)
	{
		ofLogFatalError() << "SQLite exception: " << e.what();
	}
	
}


//--------------------------------------------------------------

void ofApp::setupCV()
{

  cout << "Video Source : " << _source << endl;

  if (_source > 0){
    cam.setup(_cameraWidth, _cameraHeight, false);
    cam.setFlips(false, true);
    cam.setContrast(_contrast);
    cam.setBrightness(_brightness);
    cout << "Acquiring video from pi camera..." << endl;
    }
  else{
    cout << "Acquiring video from file : " << _video_file_name << endl;
    videoPlayer.load(_video_file_name);
    videoPlayer.setSpeed(_speed);
    videoPlayer.play();

    cout << "Play speed is " << _speed << endl;

    if (_debug > 0){
        cout << "Is playing video ? " << videoPlayer.isPlaying() << endl;
    }
  }


    pMOG2 = new BackgroundSubtractorMOG2(_history,_MOGThreshold,false);
    
    contourFinder.setMinAreaRadius(_minArea);
    contourFinder.setMaxAreaRadius(_maxArea);
    
    contourFinder.setThreshold(_contourThreshold);
    contourFinder.setFindHoles(true);
    
    tracker.setPersistence(_persistance);
    tracker.setMaximumDistance(_maxDistance);
}

//--------------------------------------------------------------
void ofApp::upload_counter(int cnt_val, string drt)
{
	string sqlstatement = 	"INSERT INTO counters (count, direction, time) VALUES('" + 
							ofToString(cnt_val) + "','" + drt + "','" + 
							ofToString(ofGetDay(),2,2,0x30) + "-" + ofToString(ofGetMonth(),2,2,0x30) + "-" +
							ofToString(ofGetYear()) + " " + ofToString(ofGetHours(),2,2,0x30) + ":" +
							ofToString(ofGetMinutes(),2,2,0x30) + ":" + ofToString(ofGetSeconds(),2,2,0x30) + "');";

	int nb = myDB->exec(sqlstatement);

}

//--------------------------------------------------------------
void ofApp::update()
{

    if (_source == 0){       // when playing video file
        videoPlayer.update();
    }

    if (lines.size() > 10) {
        lines.pop_back();
    }
    
    if (actions.size() > 10) {
        actions.pop_back();
    }

    if (_source > 0){
        frame = cam.grab();
       }
    else{
        frame = toCv(videoPlayer);
    }

    if (_debug > 0 && _source == 0){
        cout << "Is playing video ? " << videoPlayer.isPlaying() << endl;
    }

    if(!frame.empty())
    {
        resize(frame, resizeF, cv::Size(frame.size().width, frame.size().height));
        
        lightenMat = resizeF + cv::Scalar(_lightenAmount,_lightenAmount,_lightenAmount);
        
        lightenMat.copyTo(maskOutput, mask);
        
        // Activate the background subtraction
        pMOG2->operator()(maskOutput, fgMaskMOG2);
        
        // Threshold the image
        threshold(fgMaskMOG2, output, _threshold);
        
        // Blur
        blur(output, _blur);
        
        // Dilate
        dilate(output);


        // Pass through the Contour Finder
        if (ofGetFrameNum() > 200) {
            contourFinder.findContours(output);
            // Do tracking
            tracker.track(contourFinder.getBoundingRects());

            if (!bTrack){
                cout << "Tracking is started..." << endl;
                bTrack = true;
            }
        }
        else{
            if (_debug > 0){
                cout << "Frame number : " << ofGetFrameNum() << endl;
            }
        }
    }
    else{
//        cout << "Frame is empty, please check video capability..." << endl;
    }

    myLine l;
    vector<Blob>& followers = tracker.getFollowers();
    for(int i = 0; i < followers.size(); i++) {
        followers[i].setLinePosition(startLine, endLine);
        followers[i].setSizes(_minBlobSize,_midBlobSize,_maxBlobSize);
        // If the tracker returns true in open latch increment, then close latch. Then kill the tracker element.
        if (followers[i].bIn) {
            if (counterLatches[i]) {
                countIn += followers[i].howManyIn();
                count += followers[i].howManyIn();
                actions.push_front(ofToString(followers[i].howWide()));
                l.width = followers[i].howWide();
                lines.push_front(l);


                // Insert CountIn to DB
                if ( followers[i].howManyIn() > 0){
                    cout << "Incoming detected..." << followers[i].howManyIn() << " person." << endl;
                    cout << "Number : " << i << "   Wide : " << followers[i].howWide() << endl;
                    cout << "Size : " << followers[i].width << " , " << followers[i].height << endl;
                    cout << endl;

                    ofLogError() << "Incoming detected..." << followers[i].howManyIn() << " person.";
                    ofLogError() << "Number : " << i << "   Wide : " << followers[i].howWide();
                    ofLogError() << "Size : " << followers[i].width << " , " << followers[i].height;
                    ofLogError() << "";

                    upload_counter(count, "in");
                }
				
                counterLatches[i] = false;
            }
            followers[i].kill();
            counterLatches[i] = true;
        }
        // If the tracker returns true out open latch increment, then close latch. Then kill the tracker element.
        if (followers[i].bOut) {
            if (counterLatches[i]) {
                countOut += followers[i].howManyOut();
                count -= followers[i].howManyOut();
                actions.push_front(ofToString(followers[i].howWide()));
                l.width = followers[i].howWide();
                lines.push_front(l);

				// Insert CountOut to DB
				if ( followers[i].howManyOut() > 0){
				    cout << "Outgoing detected..." << followers[i].howManyOut() << " person." << endl;
                    cout << "Number : " << i << "   Wide : " << followers[i].howWide() << endl;
                    cout << "Size : " << followers[i].width << " , " << followers[i].height << endl;
                    cout << endl;

                    ofLogError() << "Outgoing detected..." << followers[i].howManyIn() << " person.";
                    ofLogError() << "Number : " << i << "   Wide : " << followers[i].howWide();
                    ofLogError() << "Size : " << followers[i].width << " , " << followers[i].height;
                    ofLogError() << "";

				    upload_counter(count, "out");
				}
				
                counterLatches[i] = false;
            }
            followers[i].kill();
            counterLatches[i] = true;
        }
    }
    
    if (followers.empty()) {
        // CounterLatches used to increment the counter
        for (int i = 0; i < 30; i++) {
            counterLatches[i] = true;
        }
    }


}
//--------------------------------------------------------------
void ofApp::drawConfig()
{
    ofPushStyle();
    ofSetColor(255);
    ofSetLineWidth(0.5);
    
    // Center
    ofLine(_cameraWidth/2,_cameraHeight/2-5,_cameraWidth/2,_cameraHeight/2+5);
    
    ofLine(_cameraWidth/2-_minBlobSize/2,_cameraHeight/2-6,_cameraWidth/2-_minBlobSize/2 ,_cameraHeight/2+6);
    ofLine(_cameraWidth/2+_minBlobSize/2,_cameraHeight/2-6,_cameraWidth/2+_minBlobSize/2 ,_cameraHeight/2+6);
    
    ofLine(_cameraWidth/2-_midBlobSize/2,_cameraHeight/2-7,_cameraWidth/2-_midBlobSize/2 ,_cameraHeight/2+7);
    ofLine(_cameraWidth/2+_midBlobSize/2,_cameraHeight/2-7,_cameraWidth/2+_midBlobSize/2 ,_cameraHeight/2+7);
    
    ofLine(_cameraWidth/2-_maxBlobSize/2,_cameraHeight/2-8,_cameraWidth/2-_maxBlobSize/2 ,_cameraHeight/2+8);
    ofLine(_cameraWidth/2+_maxBlobSize/2,_cameraHeight/2-8,_cameraWidth/2+_maxBlobSize/2 ,_cameraHeight/2+8);
    
    ofPopStyle();
}
//--------------------------------------------------------------
void ofApp::draw()
{
    if (_source == 0){       // when playing video file
        videoPlayer.draw(320, 0);
    }

    // No affect on the tracker mainly for viz
    ofSetColor(255);
    // Draw contourFinder
    contourFinder.draw();

    drawMat(frame, 320, 0,320,240);

    // Draw result of output
    drawMat(output, 0, 0,320,240);
    
    // Draw tracker
    vector<Blob>& followers = tracker.getFollowers();
    for(int i = 0; i < followers.size(); i++)
    {
        followers[i].draw();
    }
    
    string displayString = "In: " + ofToString(countIn) + " Out: " + ofToString(countOut) + " Total: " + ofToString(count);
    ofDrawBitmapStringHighlight(displayString,5,ofGetHeight()-15);
    
    // Threshold Lines
    ofPushStyle();
    ofSetColor(0, 200, 10);
    ofLine(startLine.x,startLine.y-20,endLine.x,endLine.y-20);
    ofSetColor(200, 0, 10);
    ofLine(startLine,endLine);
    ofSetColor(0, 200, 10);
    ofLine(startLine.x,startLine.y+20,endLine.x,endLine.y+20);
    ofPopStyle();
    
    drawConfig();
    
    for(int i = 0; i < lines.size(); i++) {
        ofSetColor(255,255,0,255-(i*25));
        lines[i].draw(320/2,240/2+(i*10));
    }
    
    int total = 0;
    int average = 0;
    
    for (int i = 0 ; i < actions.size(); i++) {
        total += ofToInt(actions[i]);
    }
    if (!actions.empty()) {
        average = total/actions.size();
        ofDrawBitmapStringHighlight(ofToString(average), 320,240/2);
    }



}
//--------------------------------------------------------------
void ofApp::keyPressed(int key) {   }
//--------------------------------------------------------------
void ofApp::exit() {   }
