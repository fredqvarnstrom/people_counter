import MySQLdb as mysqlinst
import sqlite3 as lite
import logging
import platform
import os

cnx = {'host': 'akeneo.cuyvtw0tno8h.us-west-1.rds.amazonaws.com',
        'username': 'people_counter',
        'password': 'qNCBZOXF9h7;126L',
        'db': 'people_counter'}
table_name = "sas_1151_s_main"

current_path = os.path.dirname(os.path.abspath(__file__))
local_DB = "counterDB.sqlite"
db_name = current_path + "/" + local_DB

log_file_name = current_path + "/myLogFile.txt"
logging.basicConfig(level=10, filename=log_file_name, format='%(asctime)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S')


def get_captured_data():
    conn = None
    try:
        print "connecting to the local database..."
        conn = lite.connect(db_name)
        curs = conn.cursor()
        curs.execute("SELECT * FROM counters")
        rows = curs.fetchall()
        conn.close()
        data_list = []
        for i in range(len(rows)):
            data_list.append(list(rows[i]))
        return data_list

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.error(" Failed to get caputred data")
    finally:
        if conn:
            conn.close()


def clear_local_db():
    con_dev = None
    try:
        con_dev = lite.connect(db_name)
        curs_tmp = con_dev.cursor()
        sql = "DELETE FROM counters;"
        curs_tmp.execute(sql)
        con_dev.commit()
        con_dev.close()

        return True

    except lite.Error as e:
        print("Error %s:" % e.args[0])
        logging.info("Error %s:" % e.args[0])
        logging.error("Deleting DB failed...")
        return False
    finally:
        if con_dev:
            con_dev.close()


def upload_data_to_aws(data):
    # connect to MySQL data base
    print 'connecting to DB...'
    print 'host name is ', cnx['host']
    print 'db name is ', cnx['db']

    server_db = mysqlinst.connect(host=cnx['host'], user=cnx['username'], passwd=cnx['password'], db=cnx['db'])
    print "connected, and now uploading data..."

    # create table if not exists
    cursor = server_db.cursor()
#    query = "CREATE TABLE IF NOT EXISTS " + table_name + " (id INTEGER PRIMARY KEY AUTO_INCREMENT" \
#                ", timestamp TEXT" \
#                ", locationID TEXT" \
#                ", event NUMERIC" \
#                ");"
#    print query

#    cursor.execute(query)
#    server_db.commit()

    # get host name
    uname = platform.uname()
    loc_id = uname[1]

    # upload data to AWS server
    try:
        for dd in data:
            var_dt = dd[3]
            if dd[2] == 'in':
                var_event = 1
            else:
                var_event = -1

            query = "INSERT INTO " + table_name + "(timestamp, locationID, event) VALUES('" \
                    + var_dt + "', '" + loc_id + "', " + str(var_event) + ");"
#            print query

            cursor.execute(query)
            server_db.commit()
        cursor.close()
        server_db.close()
        logging.info("commiting done...")
        print "Data committed"

    except mysqlinst.Error as error:
        print(error)

ddd = get_captured_data()

upload_data_to_aws(ddd)

clear_local_db()


